#------------------------------
# History stuff
#------------------------------
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

#------------------------------
# Variables
#------------------------------
export PATH="${PATH}:/home/tools/bin:/extra/ngj783/bin"
export EDITOR="vim"
export BR='http://svn.ea.mot.com/dev/bsg/branches'
export TRUNK='http://svn.ea.mot.com/dev/bsg/trunk'
export MT='http://svn.ea.mot.com/dev/bsg/branches/KA1.2_Maint'
export MANPAGER="/usr/bin/most -s"

export no_proxy="localhost,127.0.0.0/8,*.local"
export NO_PROXY="localhost,127.0.0.0/8,*.local"
export all_proxy="http://wwwgate0.mot.com:1080"
export ALL_PROXY="http://wwwgate0.mot.com:1080"
export http_proxy="http://wwwgate0.mot.com:1080"
export HTTP_PROXY="http://wwwgate0.mot.com:1080"
export ftp_proxy="http://wwwgate0.mot.com:1080"
export FTP_PROXY="http://wwwgate0.mot.com:1080"
export https_proxy="http://wwwgate0.mot.com:1080"
export HTTPS_PROXY="http://wwwgate0.mot.com:1080"

#-----------------------------
# Dircolors
#-----------------------------
#LS_COLORS='rs=0:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:tw=30;42:ow=34;42:st=37;44:ex=01;32:';
#export LS_COLORS

# color
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

#------------------------------
# Keybindings
#------------------------------
bindkey -v
typeset -g -A key
#bindkey '\e[3~' delete-char
bindkey '\e[1~' beginning-of-line
bindkey '\e[4~' end-of-line
#bindkey '\e[2~' overwrite-mode
bindkey '^?' backward-delete-char
bindkey '^[[1~' beginning-of-line
bindkey '^[[5~' up-line-or-history
bindkey '^[[3~' delete-char
bindkey '^[[4~' end-of-line
bindkey '^[[6~' down-line-or-history
bindkey '^[[A' up-line-or-search
bindkey '^[[D' backward-char
bindkey '^[[B' down-line-or-search
bindkey '^[[C' forward-char 
# for rxvt
bindkey "\e[8~" end-of-line
bindkey "\e[7~" beginning-of-line
# for gnome-terminal
bindkey "\eOH" beginning-of-line
bindkey "\eOF" end-of-line

#------------------------------
# Alias stuff
#------------------------------
#alias ls="ls --color -F"
alias ll="ls --color -lh"
alias la="ls --color -lha"
alias kl="ssh 10.245.8.203"
alias bel="ssh -l root 10.245.8.202"
alias sel="ssh -l root 10.245.8.200"

alias m3lc='mk 3pp_local_clean'
alias mlc='mk local_clean'
alias ms='m && check-coding-style'
alias kapl='platforms -c -f -t VMS610 -t IP800'
alias vmspl='platforms -c -f -t VMS610'
alias ccc="find /extra/componentcache/ -user ngj783 -exec rm -rf '{}' \;"

alias mkv='platforms -c -f -t VMS610 && mk -j 16 -C products/ip-stb/boot_image/directfb/ && after_build vms610 directfb'
alias mki='platforms -c -f -t IP800  && mk -j 16 -C products/ip-stb/boot_image/directfb/ && after_build ip800  directfb'

alias logclient='logclient -p 256-dark'
alias hm='cd /extra/ngj783'

alias hal='rcd hal/server/bcm_nexus'
alias ns='rcd platform/netconfig'
alias nst='rcd applications/testapps/netcfgtest'
alias moca='rcd extension/netconfig/mocaplugin'
alias ref='rcd hal/3pp/broadcom_refsw/7425bx'
alias fcode='fcode -c -n'
alias ker='rcd base/3pp/linux-bcm/2.6.37'
alias oob='rcd platform/oobman'
alias mmg='rcd platform/media/mediamanager'

sd()
{
  svn diff "${@}" | filterdiff | vim - -R
}

sl() {
  svn up 1>/dev/null
  svn log --stop-on-copy
}

#------------------------------
# Comp stuff
#------------------------------
zmodload zsh/complist 
autoload -Uz compinit
compinit
zstyle :compinstall filename '${HOME}/.zshrc'

#- buggy
zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:warnings' format '%BSorry, no matches for: %d%b'
#-/buggy

zstyle ':completion:*:pacman:*' force-list always
zstyle ':completion:*:*:pacman:*' menu yes select

zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*'   force-list always

zstyle ':completion:*:*:killall:*' menu yes select
zstyle ':completion:*:killall:*'   force-list always

#------------------------------
# Window title
#------------------------------
case $TERM in
    *xterm*|rxvt|rxvt-unicode|rxvt-256color|(dt|k|E)term)
		precmd () { print -Pn "\e]0;$TERM - (%L) [%n@%M]%# [%~]\a" } 
		preexec () { print -Pn "\e]0;$TERM - (%L) [%n@%M]%# [%~] ($1)\a" }
	;;
    screen)
    	precmd () { 
			print -Pn "\e]83;title \"$1\"\a" 
			print -Pn "\e]0;$TERM - (%L) [%n@%M]%# [%~]\a" 
		}
		preexec () { 
			print -Pn "\e]83;title \"$1\"\a" 
			print -Pn "\e]0;$TERM - (%L) [%n@%M]%# [%~] ($1)\a" 
		}
	;; 
esac

#------------------------------
# Prompt
#------------------------------
setprompt () {
	# load some modules
	autoload -U colors zsh/terminfo # Used in the colour alias below
	colors
	setopt prompt_subst

	# make some aliases for the colours: (coud use normal escap.seq's too)
	for color in RED GREEN YELLOW BLUE MAGENTA CYAN WHITE; do
		eval PR_$color='%{$fg[${(L)color}]%}'
	done
	PR_NO_COLOR="%{$terminfo[sgr0]%}"

	# Check the UID
	if [[ $UID -ge 1000 ]]; then # normal user
		eval PR_USER='${PR_GREEN}%n'
		eval PR_HOST='${PR_CYAN}%m'
		eval PR_USER_OP='${PR_WHITE}">>>"${PR_NO_COLOR}'
	elif [[ $UID -eq 0 ]]; then # root
		eval PR_USER='${PR_RED}%n'
		eval PR_HOST='${PR_MAGENTA}%m'
		eval PR_USER_OP='${PR_RED}%#${PR_NO_COLOR}'
	fi	

	# Check if we are on SSH or not
	if [[ -n "$SSH_CLIENT"  ||  -n "$SSH2_CLIENT" ]]; then 
		eval PR_HOST='${PR_YELLOW}%m' #SSH
	fi

	eval PR_PATH='${PR_BLUE}%~'
	eval PR_OBR='${PR_WHITE}['
	eval PR_CBR='${PR_WHITE}]'
	eval PR_TIME='${PR_OBR}%T${PR_CBR}'

	# set the prompt
	PS1=$'\n'${PR_OBR}${PR_HOST}$' '${PR_USER}$' '${PR_PATH}${PR_CBR}$'\n'${PR_TIME}${PR_USER_OP}$' '
}
setprompt

m()
{
  mk local_all
}

mm()
{
  mk --filter tree_all && m
}

mmm()
{
  local oldcwd="$(pwd)"
  local mmm_dir="$MMM_DIR"

  [ -z "$mmm_dir" ] && mmm_dir=products/ip-stb/boot_image/custom
  echo Building "$mmm_dir"
  rcd "$mmm_dir" && mm
  local ret=$?
  cd "$oldcwd"
  return $ret
}

r()
{
  local oldcwd="$(pwd)"
  cd "$(wcroot)"
  "$@"
  cd "$oldcwd"
}

rcd()
{
  local dir="$1"
  [ "${dir:0:1}" == '/' ] || dir="$(wcroot)/$dir"
  [ -d "$dir" ] || dir=$(dirname "$dir")
  cd "$dir"
}

