" Use Vim settings, rather then Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" A clean-looking font for gvim
set guifont=dejavu\ sans\ mono\ 12

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

set nobackup            " don't make backup files
set history=50          " keep 50 lines of command line history
set ruler               " show the cursor position all the time
set showcmd             " display incomplete commands
set incsearch           " do incremental searching

set list
set lcs=tab:->,trail:-,precedes:<,extends:>
set ts=2
set sw=2
set et
set nowrap
set sidescroll=5
set ch=2
set noerrorbells
set vb t_vb=
set autowrite
set autoread

" colors rastafari
syntax enable
set background=dark
colorscheme solarized
