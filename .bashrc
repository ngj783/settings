# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions

HOST="`hostname`"
#PS1='\[\e[0;35m\]\t \[\e[4;33m\]\u@\h\[\e[0;32m\] $PWD\n\[\e[1;36m\]\$\[\e[00m\] '
#PS1='\n\[\e[0;32m\]\u\[\e[m\] \[\e[1;34m\]\w\[\e[m\] \[\e[1;32m\]\n\$\[\e[m\] \[\e[1;37m\]'
PS1='\n\[\e[0;31m\]\A \e[0;33m\h\[\e[m\] \[\e[0;32m\]\u\[\e[m\] \[\e[1;34m\]\w\[\e[m\] \[\e[1;33m\]\n\$>>\[\e[m\] \[\e[1;37m\]'

export PATH=${PATH}:/home/tools/bin:/extra/ngj783/bin
export EDITOR=vim
export BR='http://svn.ea.mot.com/dev/bsg/branches'
export TRUNK='http://svn.ea.mot.com/dev/bsg/trunk'

alias lla='ll -a'
alias mk3lc='mk 3pp_local_clean'
alias mklc='mk local_clean'
alias ms='m && check-coding-style'
alias kapl='platforms -c -f -t VMS610 -t IP800'
alias vmspl='platforms -c -f -t VMS610'
alias ccc="find /extra/componentcache/ -user ngj783 -exec rm -rf '{}' \;"

alias mkv='platforms -c -f -t VMS610 && mk -j 16 -C products/ip-stb/boot_image/directfb/ && after_build vms610 directfb'
alias mki='platforms -c -f -t IP800  && mk -j 16 -C products/ip-stb/boot_image/directfb/ && after_build ip800  directfb'

alias logclient='logclient -p 256-dark'
alias hm='cd /extra/ngj783'

alias hal='rcd hal/server/bcm_nexus'
alias ns='rcd platform/netconfig'
alias nst='rcd applications/testapps/netcfgtest'
alias moca='rcd extension/netconfig/mocaplugin'
alias ref='rcd hal/3pp/broadcom_refsw/7425bx'
alias fcode='fcode -c -n'
alias ker='rcd base/3pp/linux-bcm/2.6.37'
alias oob='rcd platform/oobman'
alias mmg='rcd platform/media/mediamanager'

sd()
{
  svn diff "${@}" | filterdiff | vim - -R
}

sl() {
  svn up 1>/dev/null
  svn log --stop-on-copy
}

man() {
	env \
		LESS_TERMCAP_mb=$(printf "\e[1;31m") \
		LESS_TERMCAP_md=$(printf "\e[1;31m") \
		LESS_TERMCAP_me=$(printf "\e[0m") \
		LESS_TERMCAP_se=$(printf "\e[0m") \
		LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
		LESS_TERMCAP_ue=$(printf "\e[0m") \
		LESS_TERMCAP_us=$(printf "\e[1;32m") \
			man "$@"
}

source /home/tools/bin/_rcd
source /home/tools/bin/_mmm
