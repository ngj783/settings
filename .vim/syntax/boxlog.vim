" Vim syntax file
" Language:     KA box log file
" Maintainer:   Eugene Feoktistov <qjt647@motorola.com>
" Last Change:  $Date: 2011/12/06 16:10:00 $
" Remark: Add some color to log files produced by KA box

if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

" shut case off

syn match   fatal     "^.*Fatal:.*$"
syn match   errornonf "^.*Error:.*$"
syn match   warning   "^.*Warning:.*$"
syn match   debug     "^.*Debug:.*$"

if version >= 508 || !exists("did_boxlogs_syntax_inits")
  if version < 508
    let did_boxlogs_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

  HiLink fatal      Error
  HiLink errornonf  Constant
  HiLink warning    Type
  HiLink debug      Comment

  delcommand HiLink
endif

let b:current_syntax="boxlog"
